<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<html data-font-family="sans-serif" data-theme="spaceduck" xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<title>"<xsl:value-of select="atom:feed/atom:title"/>" Atom Feed preview</title>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1,shrink-to-fit=no" />
				<link rel="stylesheet" type="text/css" href="/assets/css/readable.min.css?v=1.1.0" />
				<link rel="stylesheet" type="text/css" href="/assets/css/themes.css" />
				<link rel="stylesheet" type="text/css" href="/assets/css/global.css" />
				<script src="/assets/js/themeSelector.js"></script>
			</head>
			<body onload="updateThemeSelector()">
				<header>
					<h1><xsl:value-of select="atom:feed/atom:title"/></h1>
					<p>Atom Feed preview</p>
				</header>
				<nav data-style="roundesque">
					<a href="/">Home</a>
					<a href="/news/">News</a>
					<a href="/financial-transparency/">Financial Transparency</a>
					<a href="https://waitlist.polymaths.social">Waitlist</a>
				</nav>
				<main>
					<p>To subscribe, just copy the URL of this page into a feed reader.</p>
					<p style="text-align: center">
						<a hreflang="en" target="_blank">
							<xsl:attribute name="href">
								<xsl:value-of select="atom:feed/atom:link/@href"/>
							</xsl:attribute>
							Visit Main Site &#x2192;
						</a>
					</p>
					<h2>Recent Posts</h2>
					<xsl:for-each select="atom:feed/atom:entry">
						<article>
							<h3 style="text-align: center">
								<a target="_blank">
									<xsl:attribute name="href">
										<xsl:value-of select="atom:link/@href"/>
									</xsl:attribute>
									<xsl:value-of select="atom:title"/>
								</a>
							</h3>
							<p style="text-align: center">
								Published:
								<time>
									<xsl:value-of select="atom:published" />
								</time>
							</p>
						</article>
					</xsl:for-each>
				</main>
			</body>
			<ol id="themeSelector" reversed="true"></ol>
		</html>
	</xsl:template>
</xsl:stylesheet>
