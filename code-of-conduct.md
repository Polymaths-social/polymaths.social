---

title: Code of Conduct (v0.2.3)

---

The purpose of this document is to protect members of the Polymaths.social community from harm in the community's spaces and to prevent bad actors within the community from harming others, either on this instance or other Fediverse instances.

## Unacceptable Behavior

The following types of behavior will not be tolerated on Polymaths.social in any form. Exhibiting them may result in immediate suspension of your account and expulsion from the community.

- Belittling, harassing, or advocating violence toward any person or group for any reason, including but not limited to race, gender or sexual identity, ethnicity, or nationality.
- Sharing of another person or group's sensitive or private information without their permission. This includes, but is not limited to, real name (if they go by a pseudonym), age, location, medical information, and any information shared in confidence, with an important exception: any information about harm to the community or indicating the likelihood of future harm **should be reported immediately** to the Code of Conduct Committee. For example, a direct message that is abusive in nature should be reported, even though it was shared in confidence.

## Disallowed Behavior

While these behaviors are not necessarily against the above values, they still are not allowed:

- Automated posting of any kind without approval from Polymaths.social staff.
- Accounts representing a project, group, or organization. (The exception being official Polymaths.social accounts.) While we acknowledge the value of these accounts existing, this community is not aimed toward providing a home for them. Please find a different instance.
- Obstructing the enforcement of this Code of Conduct or any other server rules. Since the intention of this document is to prevent harm, we will punish intentional obstruction of its enforcement the same way we would punish someone intentionally causing harm to others.
- Any illegal behavior or sharing of illegal materials, including the sharing of copyrighted material without permission.
- The sharing of NSFW ("Not Safe For Work") material.
- The sharing of material or information not suitable for minors in any public context.
- Excessive use of profanity in posts to public timelines without a Content Warning attached.

Punishment for these clauses will vary depending on the severity of the infraction.

## Enforcement

### Code of Conduct Committee

The enforcement of this Code of Conduct is performed by Polymaths.social's Code of Conduct Committee, a community-staffed team tasked by Polymaths.social's staff with enforcing the Code of Conduct and making any changes necessary to uphold the document's goal.

The current Committee consists of:

- Benjamin Hollon (Interim Committee Chair)

In any case where Committee members have a conflict of interest, they will recuse themselves if possible. Any violation of this Code of Conduct by a Committee member will result in expulsion from Polymaths.social.

Committee bylaws will be made publicly available once written. Some internal committee documents, though, such as guidelines for first responders, will remain private.

### Scope

This document applies to all messages posted on Polymaths.social and other forums of conversation run by the Polymaths.social team, regardless of visibility. In addition, if we become aware of you engaging in Unacceptable Behavior, even in places not otherwise covered by this document (for example, in another social media account), the Code of Conduct Committee may decide to take action against your account here.

If any federated servers show tendencies to produce or harbor members who exhibit Unacceptable Behavior, we will take action to silence or defederate from them.

### Consequences

Possible consequences for violating items in this Code of Conduct include (but are not exclusively limited to):

- A warning
- A conversation with a Committee member about the issue
- Post deletion
- Account suspension (temporary or permanent)

Most actions will be taken publicly, not in private, so that others can see that the Code of Conduct is being enforced. If the situation calls for it, an anonymized account will be published instead.

## Related Documents

(these documents have not yet been written)

- Expected Standards of Behavior
- Code of Conduct Committee Bylaws
