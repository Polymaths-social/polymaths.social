---
layout: homepage.njk

title: Polymaths.social
subtitle: A home for people with passions
description: At Polymaths.social, we're working to build a community where people can share their passions, form friendships, and have healthy conversations.
---

## FAQ

<details>
<summary>What is Polymaths.social?</summary>

Polymaths.social is—or, more accurately, *will* be—a Fediverse instance focused on helping people share their passions with others.

</details>

<details>
<summary>What software will Polymaths.social use?</summary>

We plan to use [GoToSocial](https://gotosocial.org). The planned launch date of early 2024 is meant to coincide with GoToSocial reaching Beta.

We've been doing extensive testing of GoToSocial and will continue doing so as future releases come out, to be sure it continues to match our needs and that all features we're waiting on are implemented.

</details>

<details>
<summary>How can I get updates on the project?</summary>

We are currently running a test instance, and you can follow the project account there, [@polymaths@alpha.polymaths.social](https://alpha.polymaths.social/@polymaths).

We also have our [news page](https://polymaths.social/news/) which you can follow via its [Atom Feed](/news/feed/).

</details>

<details>
<summary>Who is this for?</summary>

Our target audience is full of tinkerers, hobbyists, and professionals from a wide variety of fields. Basically, we're looking for people with many passions who are interested in having conversations with others about theirs.

One important note is that at this point in time we intend Polymaths.social to be for personal accounts only—no organization or project accounts allowed. We'll hammer out the details of this policy as launch approaches.

We acknowledge the value of such accounts, but Polymaths.social's goal is not to be a home for them. There are numerous other fine instances that can be your project's home.

</details>

<details>
<summary>How do I sign up?</summary>

Well… you can't. Not yet. We're currently planning to launch in early 2024 with a low user cap, then to slowly scale up as the team's confidence grows and as funding allows.

That said, you can [sign up for the waitlist](https://waitlist.polymaths.social)! We have a test instance, [Polymaths Alpha](https://alpha.polymaths.social) and are admitting many people on the waitlist to that instance. (Signing up does not guarantee admission.)

</details>

<details>
<summary>Is there any way I can help?</summary>

Other than following along on <a href="https://alpha.polymaths.social/@polymaths">the Polymaths.social Fedi account</a> and with our <a href="/news/">news updates</a> and offering feedback when it's requested, the most helpful way you can directly contribute is to <a href="https://liberapay.com/Polymaths.social/">donate to the project's on Liberapay</a>.

Benjamin is a university student with very little disposal income to fund the instance from, so every little bit of regular donation goes a long way.

Only the test instance is running at this point, but money is already being spent in preparation—for example, to register this domain name. [We publish all costs associated with Polymaths.social](/financial-transparency/) in the interests of financial transparency.

</details>
