---

title: Introducing News
author: benjaminhollon
date: 2023-12-06T17:49:50+00:00
description: "Finally, after what was truly an unacceptable interval of time, we have both a page for news updates on our site and—what is perhaps more important—an [Atom Feed](https://polymaths.social/news/feed/) for it!"
tags:
- site-update
- announcement

---

Finally, after what was truly an unacceptable interval of time, we have both a page for news updates on our site and—what is perhaps more important—an [Atom Feed](https://polymaths.social/news/feed/) for it!

## What to Expect

To begin, this will be a place for updates on the status of the project, with news about new things we'll be trying to do, updates about issues we're facing (server outages, community issues, etc.), and announcements. As time goes on, though, you can also expect:

- Us to share visions of where we want Polymaths.social to go
- Potential spotlights of great projects going on in the community (maybe as a regular occurrence? your thoughts are welcomed!)
- Updates on funding and/or calls for community input on how donated funds are used

## Other Places to Check

As you're looking for information, there are some other places that are good to check:

- [The Status Page](https://status.polymaths.social) - hosted on a separate server, this page is the place to check for outages of any Polymaths.social services.
- [The Polymaths.social Fediverse account](https://alpha.polymaths.social/@polymaths) - this official account will post smaller updates that aren't suited for a full post here
- [Our Financial Transparency page](https://polymaths.social/financial-transparency/) - this page contains up-to-date details on financial contributions to the project and how that money is spent, regularly pulled from Benjamin's private Firefly III instance, where he meticulously logs all expenses.

## A Brief Update on the Project

It's been nearly three months now, and we're incredibly grateful for how great our community's support has been. Truly, it's astounding.

For a brief summary of how things have gone:

- We launched [Polymaths Alpha](https://alpha.polymaths.social), a testing ground for both the GoToSocial software and our community-building experience as we continue prepping for our full launch in early 2024.
- We are already the largest existing GoToSocial instance, according to [Fediverse Observer](https://gotosocial.fediverse.observer/list)
- We have 36 users with a combined 27,000 status updates—truly amazing!
- After our first call for financial support, your response was overwhelming—over $200 total has been contributed, with over $5/week currently pledged in continuing donations. This is *easily* enough to launch on a strong footing.

Speaking of the financial situation, we should take a moment to look at that deeper.

First, a point that has been questioned that warrants clarification: for legal/tax reasons I (Benjamin Hollon) currently consider all donations *personal gifts* and they enter my personal bank account. I then pay for server expenses out of that same bank account.

That said, I intend to put all donated funds toward this project, though I am not *legally obligated* to do so. So far, I've donated excess funds to the GoToSocial team to help fund their amazing development work.

While current donations are enough to keep the lights running once the full instance launches, we'd love to be able to do more, as well as to have a healthy buffer to keep us stable and on secure footing. Here's our current wishlist, roughly in order of priority:

- Fully cover hosting costs (done!) with room to expand if necessary
- Have a regular additional donation from the community back to GoToSocial's development—as stated, we've begun this but believe the idea should continue and grow
- Be able to host some additional services for our community—one current idea (no promises yet!) is to provide static site hosting for community members, since the kind of people we attract are the *same* type of people who benefit from having personal/portfolio sites
- Offer financial compensation to our moderation team and staff (including myself, Benjamin), since our time is valuable.
- With available margin, have a budget to fund some community projects. For example, imagine a community literary magazine able to properly pay artists/writers who contribute; that could be an excellent opportunity for many people in our community! This is a pretty far-off goal, though.

If you're interested in helping us make some of this happen, we would be extremely grateful if you consider helping support us. You can donate [our Liberapay team](https://liberapay.com/Polymaths.social) to do so.

## Thank You!

Most of all, we want to thank all of you in our community, especially those of you who've joined us on Polymaths Alpha, along with everyone who's been supporting and cheering us on from the sidelines. It's been wonderful to have such support, and we couldn't ask for a better cheering squad.

If you have feedback, you can contact us via email at hello[at]polymaths.social. If you're interested in becoming part of our community, you can [sign up for the waitlist](https://waitlist.polymaths.social) to be first in line when we either have open spots in Polymaths Alpha or launch the full instance.

See you all again soon! Don't forget to add our [Atom Feed](https://polymaths.social/news/feed/) to your feed reader!
