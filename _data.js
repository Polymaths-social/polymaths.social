const decoder = new TextDecoder("utf-8");

// Utility functions
function readJSONSync(uri) {
	return JSON.parse(
		decoder.decode(
			Deno.readFileSync(uri)
		)
	);
}
function getQuarter(month) {
	switch (parseInt(month)) {
		case 1:
		case 2:
		case 3:
			return 1;
			break;
		case 4:
		case 5:
		case 6:
			return 2;
			break;
		case 7:
		case 8:
		case 9:
			return 3;
			break;
		case 10:
		case 11:
		case 12:
			return 4;
			break;
		default:
			throw new Error("Invalid month " + month);
			break;
	}
}

const thisQuarter = getQuarter((new Date()).getMonth() + 1);
const thisYear = (new Date()).getFullYear();

console.info('Fetching transactions from Firefly III api…');

const api = readJSONSync('./api.json');
// Firefly III API
const fireflyIIIApi = api.fireflyIII;
let response = await (await fetch(`${fireflyIIIApi.site}/api/v1/categories/${fireflyIIIApi.categoryId}/transactions?type=transactions`, {
	headers: {
		"Authorization": `Bearer ${fireflyIIIApi.personalToken}`
	}
})).json();

let transactions = [...response.data];
let page = 1;
while (response.meta.pagination.current_page !== response.meta.pagination.total_pages) {
	page++;
	response = await (await fetch(`${fireflyIIIApi.site}/api/v1/categories/${fireflyIIIApi.categoryId}/transactions?type=transactions&page=${page}`, {
		headers: {
			"Authorization": `Bearer ${fireflyIIIApi.personalToken}`
		}
	})).json();

	transactions.push(...response.data);
}

transactions = transactions.map(t => t.attributes.transactions).flat();

transactions.forEach(t => {
	t.amount = parseFloat(t.amount);
	t.date = new Date(t.date);
});
transactions = transactions.sort((a, b) => a.date - b.date);

const spending = transactions.filter(t => t.source_type !== "Revenue account");
const income = transactions.filter(t => t.source_type === "Revenue account");

const spendingThisQuarter = spending.filter(t => getQuarter(t.date.getMonth() + 1) === thisQuarter && t.date.getFullYear() === thisYear);
const incomeThisQuarter = income.filter(t => getQuarter(t.date.getMonth() + 1) === thisQuarter && t.date.getFullYear() === thisYear);

const totalSpending = spending.reduce((a, b) => {
	if (isNaN(a)) return b.amount;
	else return a + b.amount;
}, 0);
const totalIncome = income.reduce((a, b) => {
	if (isNaN(a)) return b.amount;
	else return a + b.amount;
}, 0);
const totalIncomeThisQuarter = incomeThisQuarter.reduce((a, b) => {
	if (isNaN(a)) return b.amount;
	else return a + b.amount;
}, 0);
const totalSpendingThisQuarter = spendingThisQuarter.reduce((a, b) => {
	if (isNaN(a)) return b.amount;
	else return a + b.amount;
}, 0);

const transactionsLastCalculated = new Date();

export {
	totalIncome,
	totalSpending,
	spending,
	transactionsLastCalculated,
	thisQuarter,
	thisYear,
	spendingThisQuarter,
	totalSpendingThisQuarter,
	totalIncomeThisQuarter,
};
