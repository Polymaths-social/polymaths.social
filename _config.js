import lume from "lume/mod.ts";
import nunjucks from "lume/plugins/nunjucks.ts";

const markdown = {
	options: {
		breaks: true,
		typographer: true
	}
};

const site = lume({}, { markdown });
site.use(nunjucks());

site.copy("assets");
site.copy(".htaccess");
site.copy(".well-known");
site.copy("robots.txt");

const month = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
]
site.filter(
	"date",
	(date) => {
		return month[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear()
	}
);
site.filter(
	"date_iso",
	(date) => {
		return (new Date(date)).toISOString()
	}
);
site.filter(
	"id",
	(url) => {
		return url
			.split('/')
			.filter(a => a.trim().length)
			.slice(-1)
			[0]
	}
);
site.filter(
	"timeToRead",
	(text) => {
		return Math.round(text.split(/\ |\n/g).length / 250).toLocaleString()
	}
);
site.filter(
	"prev",
	(data) => {
		const url = data[0];
		const articles = data[1];
		const index = articles.indexOf(articles.find(a => a.url === url));
		const prev = articles[index - 1]
		return prev;
	}
);
site.filter(
	"next",
	(data) => {
		const url = data[0];
		const articles = data[1];
		const index = articles.indexOf(articles.find(a => a.url === url));
		const next = articles[index + 1]
		return next;
	}
);

export default site;
