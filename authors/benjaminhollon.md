---

name: Benjamin Hollon
username: benjaminhollon
email: "me@benjaminhollon.com"

---

[Benjamin Hollon](https://benjaminhollon.com) is a storyteller and citizen of the world with far too many hobbies. Besides writing for [his blogs](https://benjaminhollon.com/blogs/), he [codes](https://codeberg.org/benjaminhollon), plays and composes music, [writes poetry and fiction](https://benjaminhollon.com/writing/), and more. He is currently studying Communications and Professional Writing at Texas A&M.

He is the founder of [Polymaths.social](https://polymaths.social) and is extremely prolific on [his Polymaths Alpha account](https://alpha.polymaths.social/@amin).
